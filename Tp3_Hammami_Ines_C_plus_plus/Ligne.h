#ifndef LIGNE_H
#define LIGNE_H


#include "Case.h"

#include <vector>
class Ligne
{   public:

    Case CaseUn;
    Case CaseDeux;
    Case CaseTrois;
    bool LigneComplete(int IdJoueur);
    Ligne Esc(Case);
    Ligne(Case CaseUn,Case CaseDeux,Case CaseTrois,Case CaseQuatre,Case CaseCinq,Case CaseSix, Case CaseSept);
    Ligne(Case CaseUn, Case CaseDeux ,Case CaseTrois);

};
#endif // LIGNE_H
