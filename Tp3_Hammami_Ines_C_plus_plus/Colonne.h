#ifndef COLONNE_H
#define COLONNE_H


#include "Case.h"
#include <vector>
class Colonne:public Case
{
public:
    Case CaseUn;
    Case CaseDeux;
    Case CaseTrois;
    bool ColonneComplete(int IdJoueur);
    Colonne(Case CaseUn,Case CaseDeux,Case CaseTrois,Case CaseQuatre)/*: (CaseUn(0,0), CaseDeux(0,0) ,CaseTrois(0,0),CaseQuatre(0,0))*/;
    Colonne(Case CaseUn,Case CaseDeux,Case CaseTrois);
    int Modifier_Valeur_Case()override;
};
#endif // COLONNE_H
