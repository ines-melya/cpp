#ifndef GRILLEMORPION_H
#define GRILLEMORPION_H

#endif // GRILLEMORPION_H
#include "Case.h"
#include "Diagonale.h"
#include <vector>   /*l'inclure sinon le vector ne marchera pas*/
#include "Ligne.h"
#include "Colonne.h"
class GrilleMorpion
{

public:

    Diagonale DiagonaleDeCasesUn = Diagonale(Case(0,0),Case(1,1),Case(2,2));
    Diagonale DiagonaleDeCasesDeux = Diagonale(Case(2,0),Case(1,1),Case(0,2));

    Colonne ColonneDeCasesUn = Colonne(Case(0,0),Case(0,1),Case(0,2));
    Colonne ColonneDeCasesDeux = Colonne(Case(1,0),Case(1,1),Case(1,2));
    Colonne ColonneDeCasesTrois = Colonne(Case(2,0),Case(2,1),Case(2,2));

    Ligne LigneDeCasesUn= Ligne (Case(0,0),Case(0,1),Case(0,2));
    Ligne LigneDeCasesDeux= Ligne (Case(1,0),Case(1,1),Case(1,2));
    Ligne LigneDeCasesTrois= Ligne (Case(2,0),Case(2,1),Case(2,2));

    bool Case_Est_Vide(Case);      /* return true si ele diag=0*/
    int DeposerUnJeton(Case, int IdJoueur);   /* case=1 , pour case qu'ondonne en param */
    bool VictoireJoueur(int IdJoueur);
    int Generer_Grille();
    void Afficher_Grille();
    int Saisir_Ordonnee_Case();
    int Saisir_Abcisse_Case();
    Case Saisir_La_Case();

    GrilleMorpion();



private:




};
