#include "GrillePuissance4.h"
#include<iostream>
using namespace std;


Case GrillePuissanceQuatre::Saisir_La_Case()
{
    int x,y;
    cout<<"saisissez les coordonnées cartésiennes de la case "<<endl;
    cin>>x>>y;
    Case La_Case_Saisie_Utilisateur= Case(x,y);
    return La_Case_Saisie_Utilisateur;
}

int GrillePuissanceQuatre::Generer_Grille()
{
    int i,j;
for (i=0;i<4;i++)
{
    for (j=0;j<7;j++)
    {
         vector<Case> nouvelleGrille ;
         Case nouvellecase= Case(i,j);
         nouvellecase.valeur=0;
         nouvelleGrille.push_back(nouvellecase);
     }

  }
    return 0;
}
void GrillePuissanceQuatre::Afficher_Grille()
{
    int i,j;
    int grille=Generer_Grille();
    for(i=0;i<4;i++)
    {
        for (j=0;j<7;j++)
        {
            cout<<grille<<endl;
        }
    }
}

bool GrillePuissanceQuatre::Case_Est_Vide(Case)
{
    Case case_joueur=Saisir_La_Case();
    if (case_joueur.valeur==0)
        {
            cout<< "La case que vous avez saisi est vide "<<endl;
            return true;
        }
    else return false;
}
int GrillePuissanceQuatre::Choix_Colonne_Jeu()
{
    int NumeroColonneJoueur;
    cout<<"Selectionner le numero de la colonne où vous souhaitez joué :"<<endl;
    cin>>NumeroColonneJoueur;
    NumeroColonneJoueur--;
    return NumeroColonneJoueur;
}
Case GrillePuissanceQuatre::Case_Basse_Vide()
{
    Case case_joueur=Saisir_La_Case();
    case_joueur.x=Choix_Colonne_Jeu();
    while (case_joueur.valeur==0)
        {
            case_joueur.y--;
        }
    return  case_joueur;
}
int GrillePuissanceQuatre::Changer_De_Joueur()
{
    Case Case_du_jeu=Saisir_La_Case();
    int IdJoueur=Case_du_jeu.valeur;
    int IdJoueurUn=1;
    int IdJoueurDeux=2;
    switch (IdJoueur)
    {
    case 1: Case_du_jeu.valeur=IdJoueur;
            IdJoueur=IdJoueurDeux;
        break;
    case 2: Case_du_jeu.valeur=IdJoueur;
            IdJoueur=IdJoueurUn;
        break;
    }
    return IdJoueur;
}
int GrillePuissanceQuatre::DeposerUnJeton(int IdJoueur)
{
    Case case_joueur=Case_Basse_Vide();
    if (Case_Est_Vide(case_joueur))return true;
    case_joueur.valeur=IdJoueur;
    IdJoueur=Changer_De_Joueur();
    return IdJoueur;
}

bool GrillePuissanceQuatre::VictoireJoueur(int IdJoueur)
{
    if (Ligne_Un.LigneComplete(IdJoueur))return true;
    if (Ligne_Deux.LigneComplete(IdJoueur))return true;
    if (Ligne_Trois.LigneComplete(IdJoueur))return true;
    if (Ligne_Quatre.LigneComplete(IdJoueur))return true;
    if (ColonneUn.ColonneComplete(IdJoueur))return true;
    if (ColonneDeux.ColonneComplete(IdJoueur))return true;
    if (ColonneTrois.ColonneComplete(IdJoueur))return true;
    if (ColonneQuatre.ColonneComplete(IdJoueur))return true;
    if (ColonneCinq.ColonneComplete(IdJoueur))return true;
    if (ColonneSix.ColonneComplete(IdJoueur))return true;
    if (ColonneSept.ColonneComplete(IdJoueur))return true;
    if (Diagonale_Un.DiagonaleComplete(IdJoueur))return true;
    if (Diagonale_Deux.DiagonaleComplete(IdJoueur))return true;
    if (Diagonale_Trois.DiagonaleComplete(IdJoueur))return true;
    if (Diagonale_Quatre.DiagonaleComplete(IdJoueur))return true;
    if (Diagonale_Cinq.DiagonaleComplete(IdJoueur))return true;
    if (Diagonale_Six.DiagonaleComplete(IdJoueur))return true;
    if (Diagonale_Sept.DiagonaleComplete(IdJoueur))return true;
    if (Diagonale_Huit.DiagonaleComplete(IdJoueur))return true;
    else return false;

}
