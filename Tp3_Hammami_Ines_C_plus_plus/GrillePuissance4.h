#ifndef GRILLEPUISSANCE4_H
#define GRILLEPUISSANCE4_H
#include "Case.h"
#include "Colonne.h"
#include"Diagonale.h"
#include"Ligne.h"


class GrillePuissanceQuatre
{
public:
    Ligne Ligne_Un =Ligne(Case(0,0),Case(1,0),Case(2,0),Case(3,0),Case(4,0),Case(5,0),Case(6,0));
    Ligne Ligne_Deux =Ligne(Case(0,1),Case(1,1),Case(2,1),Case(3,1),Case(4,1),Case(5,1),Case(6,1));
    Ligne Ligne_Trois =Ligne(Case(0,2),Case(1,2),Case(2,2),Case(3,2),Case(4,2),Case(5,2),Case(6,2));
    Ligne Ligne_Quatre =Ligne(Case(0,3),Case(1,3),Case(2,3),Case(3,3),Case(4,3),Case(5,3),Case(6,3));

    Diagonale Diagonale_Un = Diagonale(Case(0,0),Case(1,1),Case(2,2),Case(3,3));
    Diagonale Diagonale_Deux = Diagonale(Case(1,0),Case(2,1),Case(3,2),Case(4,3));
    Diagonale Diagonale_Trois = Diagonale(Case(2,0),Case(3,1),Case(4,2),Case(5,3));
    Diagonale Diagonale_Quatre = Diagonale(Case(3,0),Case(4,1),Case(5,2),Case(6,3));
    Diagonale Diagonale_Cinq = Diagonale(Case(0,0),Case(1,1),Case(2,2),Case(3,3));
    Diagonale Diagonale_Six = Diagonale(Case(3,0),Case(2,1),Case(1,2),Case(0,3));
    Diagonale Diagonale_Sept = Diagonale(Case(4,0),Case(3,1),Case(2,2),Case(1,3));
    Diagonale Diagonale_Huit = Diagonale(Case(6,0),Case(5,1),Case(4,2),Case(3,3));

    Colonne ColonneUn = Colonne(Case(0,0),Case(0,1),Case(0,2),Case(0,3));
    Colonne ColonneDeux = Colonne(Case(1,0),Case(1,1),Case(1,2),Case(1,3));
    Colonne ColonneTrois = Colonne(Case(2,0),Case(2,1),Case(3,2),Case(4,3));
    Colonne ColonneQuatre = Colonne(Case(3,0),Case(3,1),Case(3,2),Case(3,3));
    Colonne ColonneCinq = Colonne(Case(4,0),Case(4,1),Case(4,2),Case(4,3));
    Colonne ColonneSix = Colonne(Case(5,0),Case(5,1),Case(5,2),Case(5,3));
    Colonne ColonneSept = Colonne(Case(6,0),Case(6,1),Case(6,2),Case(6,3));

    void Afficher_Grille();
    int Deposer_Jeton();
    bool Case_Est_Vide(Case);      /* return true si ele diag=0*/
    int DeposerUnJeton(int IdJoueur);   /* case=1 , pour case qu'ondonne en param */
    bool VictoireJoueur(int IdJoueur);
    int Generer_Grille();
    int Changer_De_Joueur();
    int Choix_Colonne_Jeu();
    Case Case_Basse_Vide();
    int Saisir_Ordonnee_Case();
    int Saisir_Abcisse_Case();
    Case Saisir_La_Case();
private:



};
#endif // GRILLEPUISSANCE4_H
