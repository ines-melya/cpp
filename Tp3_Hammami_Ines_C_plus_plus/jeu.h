#ifndef JEU_H
#define JEU_H
#include "Case.h"
#include "GrilleMorpion.h"
class Jeu
{
public:
   GrilleMorpion Grille;
   int AtonTour(Case CaseduJoueur, int IdJoueur );
    Case Saisir_Coordonnes_Case();
    bool Verification_Coordonnees_Saisies(Case);
    bool Case_Saisie_Est_Vide(Case);
    bool La_Grille_Est_Pleine(Case);
    int Tour_De_Quel_Joueur();
    Jeu();
};

#endif // JEU_H
