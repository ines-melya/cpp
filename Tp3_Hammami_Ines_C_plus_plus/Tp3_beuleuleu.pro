TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        Case.cpp \
        Colonne.cpp \
        Diagonale.cpp \
        GrilleMorpion.cpp \
        GrillePuissance4.cpp \
        Ligne.cpp \
        jeu.cpp \
        main.cpp

HEADERS += \
    Case.h \
    Colonne.h \
    Diagonale.h \
    GrilleMorpion.h \
    GrillePuissance4.h \
    Ligne.h \
    jeu.h
