#ifndef DIAGONALE_H
#define DIAGONALE_H


#include <iostream>
#include "Case.h"
#include <vector>
using namespace std;

class Diagonale:public Case
{
public:

    Case CaseUn;
    Case CaseDeux;
    Case CaseTrois;
    Diagonale Diagonale_puissance_quatre(Case);
    bool DiagonaleComplete(int IdJoueur);
    Diagonale(Case CaseUn,Case CaseDeux,Case CaseTrois,Case CaseQuatre);
    Diagonale(Case CaseUn,Case CaseDeux,Case CaseTrois);
};
#endif // DIAGONALE_H
