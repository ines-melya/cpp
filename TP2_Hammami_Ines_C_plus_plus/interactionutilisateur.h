#ifndef INTERACTIONUTILISATEUR_H
#define INTERACTIONUTILISATEUR_H
#include"Forme.h"
#include"Point.h"
#include"Rectangle.h"
#include"Triangle.h"
#include"cercle.h"
#include <iostream>

class InteractionUtilisateur
{
public:
    float Demander_Largeur_Utilisateur();
    float Demander_Longueur_Utilisateur();
    Point Demander_Utilisateur_Coordonnees_Points_Un();
    Point Demander_Utilisateur_Coordonnees_Points_Deux();
    Point Demander_Utilisateur_Coordonnees_Points_Trois();
    int Demander_Diametre_Utilisateur();
    float Demander_float_utilisateur(std::string nom);
    InteractionUtilisateur();
    InteractionUtilisateur(Point Point_Un,Point Point_Deux,Point Point_Trois);

private:
    Point Point_Un;
    Point Point_Deux;
    Point Point_Trois;
};

#endif // INTERACTIONUTILISATEUR_H
