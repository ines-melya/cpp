#include "interactionutilisateur.h"
#include<iostream>
using namespace std;

float InteractionUtilisateur::Demander_float_utilisateur(std::string nom)
{
    string monnombre;
    bool isNumber=false;
    float Largeur;
    while (isNumber!=true)
      {

        isNumber=true;   /*Pour eviter que isnumber toujours false*/
        cout<<" quelle est la " << nom <<": "<<endl;
        cin>>monnombre;
        try {
            Largeur=stof(monnombre);

        }  catch (invalid_argument exception) /*si tombe sur erreur envoi sur catch, type d'erreur qu'on catch invalid argument , donc on éxécute le code de suivant*/
        {
            isNumber=false;
            cout<<"La "<<nom<< " saisi n'est pas un nombre!"<<endl;
        }
    }
    cout<< Largeur <<"est bien rempli de chiffres! "<<endl;

    return Largeur;
}
float InteractionUtilisateur::Demander_Largeur_Utilisateur()
{
    return Demander_float_utilisateur("Largeur");
}
float InteractionUtilisateur::Demander_Longueur_Utilisateur()
{
    return Demander_float_utilisateur("Longueur");
}
int InteractionUtilisateur::Demander_Diametre_Utilisateur()
{
    return Demander_float_utilisateur("Diametre");
}
Point InteractionUtilisateur::Demander_Utilisateur_Coordonnees_Points_Un()
{
    cout<<"rentrer l'abcisse du point A"<<endl;                 /*on demande a l'utilisateur de rentrer les coordonnées cartesiennes des points du triangle*/
    cin >>Point_Un.x;
    cout<<"rentrer l'ordonnée du point A"<<endl;
    cin >>Point_Un.y;
    return Point_Un;
}
Point InteractionUtilisateur::Demander_Utilisateur_Coordonnees_Points_Deux()
{
    cout<<"rentrer l'abcisse du point B"<<endl;
    cin >>Point_Deux.x;
    cout<<"rentrer l'ordonnee du point B"<<endl;
    cin >>Point_Deux.y;
   return Point_Deux;
}
Point InteractionUtilisateur::Demander_Utilisateur_Coordonnees_Points_Trois()
{
    float num;	while (cout << "Enter a number: " && !(cin >> num) )
       { cout << "That''s not valid input. Try again" << endl;
        cin.clear();
            }
    cout<<"rentrer l'abcisse du point C"<<endl;
    cin >>Point_Trois.x;
    cout<<"rentrer l'ordonnee du point C"<<endl;
    cin >>Point_Trois.y;
    return Point_Trois;
}
InteractionUtilisateur::InteractionUtilisateur()
{

}
InteractionUtilisateur::InteractionUtilisateur(Point Point_Un,Point Point_Deux,Point Point_Trois):Point_Un(Point_Un),Point_Deux(Point_Deux),Point_Trois(Point_Trois)
{

}
