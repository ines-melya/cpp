#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "Point.h"
#include"Forme.h"
#include"interactionutilisateur.h"

class Rectangle:public Forme
{
public:
     int GetLongueur() const  /* Un get permet d'acceder à une valeur privée en mode public  */
     {
        return  Longueur;
     }
    int GetLargeur() const    /* Comme les getters sont des predicats et ne modifie pas l'objet on peut mettre const(normalement)*/
     {
        return  Largeur;
     }
    void SetLongueur(int _Longueur)
        {
           Longueur=_Longueur;
        }                                            /*  Un Set permet de modifier une valeur privée  */
    void SetLargeur(int _Largeur)
        {
         Largeur=_Largeur;   /*plutot en public comme on fait r*/
        }                         /* Un get permet d'acceder à une valeur privée en mode public  */
    void Afficher_Perimetre_Surface_CoinSuperieur();
    float Surface() override;
    float Perimetre() override;
    Point Coin_Sup();

    Rectangle();
private:
    int Longueur;
    float Largeur;


};
#endif // RECTANGLE_H
