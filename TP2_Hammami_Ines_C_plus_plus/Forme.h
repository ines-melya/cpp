#ifndef FORME_H
#define FORME_H



class Forme
{

public:
    virtual float Surface()=0;
    virtual float Perimetre()=0;
};
#endif // FORME_H
