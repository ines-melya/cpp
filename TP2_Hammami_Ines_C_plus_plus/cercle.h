#ifndef CERCLE_H
#define CERCLE_H
#include "Point.h"
#include"Forme.h"
class Cercle:public Forme
{
public:
    int getDiametre()
    {
        return Diametre;
    }
    void setDiametre(int diametre)
    {
        Diametre=diametre;
    }
    Point getCentre()
    {
        return Centre;
    }
    void setCentre(Point newCentre)
    {
        this->Centre=newCentre ;
    }
    int Demander_Diametre_Utilisateur();
    float Calcul_surface_Cercle();
    void Afficher_Diametre_Surface_Perimetre_Console();
    float perimetre_Du_Cercle();
    float distance(Point un,Point deux);
    bool areyouin(Point un, Point deux);
    float Surface();
    float Perimetre();
    Cercle();
    Cercle(double Diametre, Point Centre);
private:
    double Diametre;
    Point Centre;
    float const pi=3.1415;
};

#endif // CERCLE_H
