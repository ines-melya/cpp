#include <iostream>
using namespace std;
#include "Rectangle.h"
#include "Point.h"
#include"interactionutilisateur.h"
#include <cmath>

float Rectangle::Perimetre()
{
    double perimetre = (Longueur*2)+(Largeur*2);
    return perimetre;
}
float Rectangle::Surface()
{
    double Surface=Longueur*Largeur;
     return Surface;
}
Point Rectangle::Coin_Sup()
{
    Point CoinSup;
    CoinSup.x=0;
    CoinSup.y=Largeur;
    return CoinSup;
}
void Rectangle::Afficher_Perimetre_Surface_CoinSuperieur()
{
    double perimetre=Perimetre();
    double surface=Surface();
    Point CoinSup=Coin_Sup();
    cout<< "Le perimetre vaut:  "<< perimetre <<endl;
    cout<< "La Surface vaut:  "<< surface <<endl;
    cout<< "le coin superieur est a "<< "("<<CoinSup.x<<","<< CoinSup.y<<")"<<endl;
}
Rectangle::Rectangle()
{
    Largeur=InteractionUtilisateur().Demander_Largeur_Utilisateur();
    Longueur=InteractionUtilisateur().Demander_Longueur_Utilisateur();

}
