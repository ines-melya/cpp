#include "Triangle.h"
#include <iostream>
using namespace std;
#include "Point.h"
#include"Forme.h"
#include "interactionutilisateur.h"
#include <cmath>

bool Triangle::Verifier_Points_Triangle_Differents() const
{
    if (Point_Un.x==Point_Deux.x && Point_Un.y==Point_Deux.y)return false;
    if (Point_Deux.x==Point_Trois.x && Point_Deux.y == Point_Trois.y) return false;
    if (Point_Trois.x==Point_Un.x && Point_Trois.y==Point_Un.y) return false;
    else return true;
}
float Triangle::Calcul_Cote_Triangle_AB() const
{
    if (Verifier_Points_Triangle_Differents())return true;
    double soustraction_abcisse=Point_Un.x-Point_Deux.x;
    double soustraction_ordonnee=Point_Un.y-Point_Deux.y;
    double soustractionOrdonneeCarre=soustraction_ordonnee*soustraction_ordonnee;
    double soustractionAbcisseCarre=soustraction_abcisse*soustraction_abcisse;
    double AB=sqrt(soustractionAbcisseCarre+soustractionOrdonneeCarre);
    return AB;
}
float Triangle::Calcul_Cote_Triangle_BC() const
{   if (Verifier_Points_Triangle_Differents())return true;
    double soustraction_abcisse=Point_Deux.x-Point_Trois.x;
    double soustraction_ordonnee=Point_Deux.y-Point_Trois.y;
    double soustraction_abcisse_carre=soustraction_abcisse*soustraction_abcisse;
    double soustraction_ordonnee_carre=soustraction_ordonnee*soustraction_ordonnee;
    double BC=sqrt(soustraction_abcisse_carre+soustraction_ordonnee_carre);
    return BC;
}
float Triangle::Calcul_Cote_Triangle_CA() const
{    if (Verifier_Points_Triangle_Differents())return true;
     double soustraction_abcisse=Point_Trois.x-Point_Un.x;
     double soustraction_ordonnee=Point_Trois.y-Point_Un.y;
     double soustraction_abcisse_carre=soustraction_abcisse*soustraction_abcisse;
     double soustraction_ordonnee_carre=soustraction_ordonnee*soustraction_ordonnee;
     double CA=sqrt(soustraction_abcisse_carre+soustraction_ordonnee_carre);
    return CA;
}
bool Triangle::Est_Il_Isocele()const
{
    if (Est_Il_Equilateral())return false ;
    {
        if (AB==BC)
        {
        cout<<"le triangle est isocele car AB=AC"<<endl;
        return true;
        }
        else if (BC==CA)
        {
        cout<<"le triangle est isocele car BC=CA"<<endl;
        return true;
        }
        else if (CA==AB)
        {
        cout<<"le triangle est isocele car AB=CA"<<endl;
        return true;
        }
    else return false;
  }
}
float Triangle::Trouver_Base() const
{
    double base;
    if ((AB>BC)&&(AB>CA))
    {                              /* si ab est plus grand que les deux autres cotes c'est donc ab la base de ce triangle*/
        base=AB;
        cout<<"la base de ce triangle est AB "<<endl;
        return base;
    }
    else if ((BC>AB)&&(BC>CA))
    {
        base=BC;
        cout<<"la base de ce triangle est BC "<<endl;
        return base;
    }
    else
    {
        base=CA;
        cout<<"la base de ce triangle est CA "<<endl;
        return base;
     }
}
bool Triangle::Est_Il_Equilateral()const
{
    if ((AB==BC)&&(BC==CA)&&(CA==AB))
    {
        return true;
    }
    else return false;
}
bool Triangle::Est_Il_Rectangle()const
{
    float Base=Trouver_Base();
    if ((Base)==sqrt(BC+CA) | (Base)==sqrt(AB+CA) | (Base)==sqrt(AB+BC))
    {
        return true;
    }
    else return false;

}
float Triangle::Calculer_Hauteur()const
{
    double hauteur;   /*Grace a une autre tres belle formule on obtient la hauteur juste à l'aide de la longueur des cotés*/
    hauteur=sqrt(((BC+AB+CA/2)*((BC+AB+CA/2)-BC)*((BC+AB+CA/2)-CA)*((BC+AB+CA/2)-AB)))*(2/BC);
    return hauteur;
}
void Triangle::Afficher_Base_Hauteur_Surface_Longueurs()
{
    double base=Trouver_Base();
    double Hauteur=Calculer_Hauteur();
    double surface=Surface();
    double perimetre = Perimetre();
    cout<<"Voici la longueur AB: "<<AB<<endl;
    cout<<"Voici la longueur BC: "<<BC<<endl;
    cout<<"Voici la longueur CA: "<<CA<<endl;
    cout<<"voici le perimetre : "<<perimetre<<endl;
    cout<<"Voici la longueur de la base: "<<base<<endl;
    cout<<"Voici la hauteur: "<<Hauteur<<endl;
    cout<<"Voici la surface: "<<surface<<endl;
    if ((Est_Il_Equilateral())== true){cout <<"il est equilateral"<<endl;}
    if ((Est_Il_Isocele()==true)){cout<<"il est isocele"<<endl;}
    if ((Est_Il_Rectangle()==true)){cout<<"il est rectangle"<<endl;}
}
float Triangle::Surface()
{
    double hauteur = Calculer_Hauteur();
    double base = Trouver_Base();
    double Surface;
    Surface =(hauteur*base)/2;
    return Surface;
}
float Triangle::Perimetre()
{
    float perimetre= AB+BC+CA;
    return perimetre;
}
Triangle::Triangle()
{
    Point_Un= InteractionUtilisateur().Demander_Utilisateur_Coordonnees_Points_Un();
    Point_Deux= InteractionUtilisateur().Demander_Utilisateur_Coordonnees_Points_Deux();
    Point_Trois = InteractionUtilisateur().Demander_Utilisateur_Coordonnees_Points_Trois();
    AB = Calcul_Cote_Triangle_AB();
    CA = Calcul_Cote_Triangle_CA();
    BC = Calcul_Cote_Triangle_BC();
}
/*Triangle::Triangle(Point Point_Un,Point Point_Deux,Point Point_Trois):Point_Un(Point_Un),Point_Deux(Point_Deux),Point_Trois(Point_Trois)
{
    Point_Un= InteractionUtilisateur().Demander_Utilisateur_Coordonnees_Points_Un();
    Point_Deux= InteractionUtilisateur().Demander_Utilisateur_Coordonnees_Points_Deux();
    Point_Trois = InteractionUtilisateur().Demander_Utilisateur_Coordonnees_Points_Trois();

}*/
