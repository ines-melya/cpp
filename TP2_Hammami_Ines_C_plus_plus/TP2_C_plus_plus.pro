TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        Rectangle.cpp \
        Triangle.cpp \
        cercle.cpp \
        interactionutilisateur.cpp \
        main.cpp

HEADERS += \
    Forme.h \
    Point.h \
    Rectangle.h \
    Triangle.h \
    cercle.h \
    interactionutilisateur.h
