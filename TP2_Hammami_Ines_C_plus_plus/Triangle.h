#ifndef TRIANGLE_H
#define TRIANGLE_H
#include"Point.h"
#include"Forme.h"
#include"interactionutilisateur.h"
class Triangle:public Forme
{
public:
    Point GetPoint_Un()const
    {
        return Point_Un;
    }
    Point  GetPoint_Deux()const
    {
        return Point_Deux;
    }
    Point GetPoint_Trois()const
    {
        return Point_Trois;
    }
    float GetAB()const
    {
        return AB;
    }
    float GetBC()const
    {
        return BC;
    }
    float GetCA()const
    {
        return CA;
    }
    void SetPoint_Un( Point _Point_Un)
    {
         Point_Un=_Point_Un;
    }
    void SetPoint_Deux(Point _Point_Deux)
    {
        Point_Deux =_Point_Deux;
    }
    void SetPoint_Trois(Point _Point_Trois)
    {
        Point_Trois=_Point_Trois;
    }
    float Calcul_Cote_Triangle_AB()const;
    float Calcul_Cote_Triangle_CA()const;
    float Calcul_Cote_Triangle_BC()const;
    float Trouver_Base()const;
    float Calculer_Hauteur()const;
    float Surface() override;
    float Perimetre() override;
    bool Verifier_Points_Triangle_Differents()const;
    bool Est_Il_Isocele()const;
    bool Est_Il_Rectangle()const;
    bool Est_Il_Equilateral()const;
    void Afficher_Base_Hauteur_Surface_Longueurs();
    Triangle();
    Triangle(Point Point_Un,Point Point_Deux,Point Point_Trois);

private:
    Point Point_Un;
    Point Point_Deux;
    Point Point_Trois;
    float AB,BC,CA;
};
#endif // TRIANGLE_H
