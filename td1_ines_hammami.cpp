#include <iostream>
using namespace std;
#include <random>
#include <cstdlib>
#include <time.h>

// Partie 1

//Question 1 :
void Addition_Console(){
   int Premier_nombre,Deuxieme_nombre,somme;
   cout << "veuillez entrer a et b: "<<endl  ;
   cin >> Premier_nombre>>Deuxieme_nombre;
   somme=Premier_nombre+Deuxieme_nombre;
   cout << "resultat= " << somme<<endl;

}

//Question 2:
void Inverse_Les_nombres_Saisis (){
    int Premier_nombre,Deuxieme_nombre,Var_Temporaire;
    cout<< "rentrer un premier nombre puis un deuxième : ";
    cin >>Premier_nombre>>Deuxieme_nombre;
    Var_Temporaire=Premier_nombre;
    Premier_nombre=Deuxieme_nombre;
    Deuxieme_nombre=Var_Temporaire;
    cout << "Le premier nombre vaut dorénavant : "<< Premier_nombre << "Le Deuxieme nombre vaut dorénavant :" <<Deuxieme_nombre << endl;

}

//Question 3:
void Somme_de_pointeurs() {
    int *pointeur_un=0,*pointeur_deux=0;
    int somme_des_pointeurs;
    cout << "rentrer a et b"<< endl;
    cin >>*pointeur_un>>*pointeur_deux;
    somme_des_pointeurs= *pointeur_un+*pointeur_deux;
    cout << " la somme vaut= " << somme_des_pointeurs<< endl;

}

//Question 3 avec les références:
void Somme_De_Reference () {
    int Nombre_un,Nombre_deux,Somme;
    cout << "rentrer a et b";
    cin >>Nombre_un>>Nombre_deux;
    Somme= Nombre_un + Nombre_deux;
    int &reference_somme=Somme;
    cout << " la somme vaut= " << &reference_somme<< endl;

}

// Question 4:

void Tri_Tableau_Rempli_Hasard (){
    int Variable_temporaire=5;

    srand(time(NULL));
    int tab [Variable_temporaire];
    int i=0;
    while (i=0,i<=Variable_temporaire)
    {
        tab[i]=(rand()%100);        // Ici j'ai choisi d'avoir des valeurs aléatoires dans le tableau
        i++;
        cout << tab[i]<< endl;
    }


    for (int i=0;i<=Variable_temporaire;i++)
    {
            while (tab[i]<tab[i+1])
            {
                Variable_temporaire=tab[i];
                tab[i]=tab[i+1];
                tab[i+1]=Variable_temporaire;
            }
    }
    cout<< "Voici le tableau trier dans l'ordre croissant"<<endl;
    cout <<tab[i]<<endl;



}


//Question 4 bonus :

int Tri_Croissant(int tableau[])
{
    int Variable_temporaire,Taille_du_tableau;
    for (int i=0;i<Taille_du_tableau;i++)
         {
             while (tableau[i]<tableau[i+1])  /*mettre fonction a part*/
             {
                 Variable_temporaire=tableau[i];
                 tableau[i]=tableau[i+1];
                 tableau[i+1]=Variable_temporaire;
             }
              cout <<tableau[i]<<endl;
          }
}
int Tri_Decroissant(int tableau[])
{
    int Variable_temporaire,Taille_du_tableau;
    for (int i=0;i<Taille_du_tableau;i++)
    {
        while (tableau[i]>tableau[i+1])
        {
            Variable_temporaire=tableau[i];
            tableau[i]=tableau[i+1];
            tableau[i+1]=Variable_temporaire;
        }
        return tableau[i];
    }
}

bool Choix_Tri_Tableau_Utilisateur()
{
    char option;

    cout << "comment souhaiter vous trier le tableau ? Tapez c pour croissant et d pour décroissant : "<< endl;
    cin>> option;
    if (option=='c')
    {
        return true;
    }
    else return false;
}
int Choix_Taille_Tableau_Utilisateur()
{
    int Taille_du_tableau;
    cout<<"Veuillez rentre la taille du tableau: "<<endl;
    cin>>Taille_du_tableau;
    return Taille_du_tableau;
}
void Remplissage_Aléatoire_Tableau (int Taille_du_tableau){
    int i;

    srand(time(NULL));

    int tableau [Taille_du_tableau];
    while (i=0,i<=Taille_du_tableau)
    {
        tableau[i]=(rand()%100);        // Ici j'ai choisi d'avoir des valeurs aléatoires dans le tableau
        i++;
        cout << tableau[i]<< endl;
    }
 }


// Partie 2 : Jeu de tennis

int Echange_Tennis( )
{
    int score_decisif=4;
    int score_joueur_un=0;
    int score_joueur_deux=0;
    while (score_joueur_un!=score_decisif or score_joueur_deux!=score_decisif)
    {
        for (int i=0;i<8;i++)
        {
            srand(time(NULL));
            int victoire_echange_aleatoire=rand()%1; // nombre aleatoire , 0 ou 1, sert à savoir qui gagne l'échange
            switch (victoire_echange_aleatoire)
            {
                     case 0: score_joueur_un=score_joueur_un++;
                            break;
                     case 1: score_joueur_deux=score_joueur_deux++;
                            break;

            }
        }
    }
 return score_joueur_deux,score_joueur_un;
}
int Echange_decisif(int score_joueur_un,int score_joueur_deux)
{
    int  Echange_decisif=rand()%1; // nombre aleatoire , 0 ou 1
    for (int i=0;i<2;i++)
    {
     switch (Echange_decisif)
     {
              case 0: score_joueur_un=score_joueur_un++;
              case 1: score_joueur_deux=score_joueur_deux++;
     }
    }
    return score_joueur_deux,score_joueur_un;
}
int Egalite_tennis(int score_joueur_un,int score_joueur_deux)
{
    Echange_Tennis();
    if (score_joueur_deux==score_joueur_un)
    {
       Echange_decisif( score_joueur_un,score_joueur_deux);
      return score_joueur_un,score_joueur_deux;

    }
}
void Afficher_Victoire_tennis_Console(int score_joueur_un,int score_joueur_deux)
{
    Echange_Tennis();
    int score_decisif=4;
    if (score_joueur_un>score_joueur_deux)
    {
        cout<< "le score est: "<<score_joueur_un<<" - "<<score_joueur_deux<<endl;
        cout<< "le joueur 1 à gagné !"<<endl;
    }
    if (score_joueur_deux==score_decisif and score_joueur_un!=score_decisif)
    {
        cout<< "le score est: "<<score_joueur_deux <<" - "<<score_joueur_un<<endl;
        cout<< "le joueur 2 à gagné !"<<endl;
    }
}

//Partie 3

// Question 3.1.1 :
void bonjour_prenom_Console() {
    char prenom;
    cout << "quel est votre prenom  "<< endl;
    cin >> prenom;
    cout << " Bonjour "<< prenom << endl;
      }

//Question 3.1.2 :
void bonjour_nom_prenom_Console(){
    char prenom;
    char nom;
    cout << "Quel est votre prenom "<< endl << "quel est votre nom " << endl ;
    cin >> prenom >> nom;
    cout << "Bonjour " << nom << prenom<<endl;
}

//Question 3.1.2 BONUS :
int Demander_Nom_Prenom_Utilisateur(char nom, char prenom)
{

    cout << "Quel est votre prenom "<< endl << "quel est votre nom " << endl ;
    cin >> prenom >> nom;
    return nom,prenom;
}
int bonjour_nom_prenom_formater(){
    Demander_Nom_Prenom_Utilisateur(nom,prenom); /*question : comment utiliser le nom et prenom ici retourner dans la fonction demander*/
    char prenom_tableau[0];
    prenom_tableau[0]=prenom;
    char nom_tableau[0];
    nom_tableau[0]=nom;
    int premiere_lettre_nom=0;
    int premiere_lettre_prenom=0;
    char nom_formater;
    char prenom_formater;
    while (nom_tableau[premiere_lettre_nom]<1)
       {
        nom_formater=nom_tableau[premiere_lettre_nom];
        putchar (toupper(premiere_lettre_nom));
        premiere_lettre_nom=premiere_lettre_nom+1;
        return nom_formater;
       }
    while (prenom_tableau[premiere_lettre_prenom]<1){
        prenom_formater=prenom_tableau[premiere_lettre_prenom];
        putchar (toupper(premiere_lettre_prenom));
        return prenom_formater;

    }
}
void Afficher_Nom_Prenom_Formater_Console()
{
    char prenom,nom;
    cout << "Quel est votre prenom "<< endl << "quel est votre nom " << endl ;
    cin >> prenom >> nom;
    bonjour_nom_prenom_formater();  /*question : comment utiliser le nom_formater et prenom_formater retourner par la fonction bonjour dans l'affichage*/
    cout << "Bonjour " << nom_formater << prenom_formater;
}
//Question 3.2.1:
void Deviner_nombre_mystere_ordinateur(){
    int nombre_mystere_ordinateur;
    int nombre_utilisateur;
    cout << " l'ordinateur va choisir un nombre mystere entre 0 et 1000 "<< endl;
    srand(time(NULL));
    nombre_mystere_ordinateur = rand()%1000+1;
    cout << "Saisissez ce que vous pensez etre le nombre mystere (entre 0 et 1000)"<<endl;
    cin >> nombre_utilisateur;
    if (nombre_utilisateur>nombre_mystere_ordinateur){
        cout << "votre nombre est plus grand que le nombre mystère"<< endl;
                }
    if (nombre_utilisateur<nombre_mystere_ordinateur){
        cout << "votre nombre est plus petit que le nombre mystère"<< endl;
    }
    else {
        cout << "Bravo champion "<< endl;
    }
}
//Question 3.2.2 :
void nombre_mystere_essais(){
    int nombre_mystere_ordinateur;
    int nombre_utilisateur;
    int compteur_essai_utlisateur;

    cout << " l'ordinateur va choisir un nombre mystere entre 0 et 1000 "<< endl;
    srand(time(NULL));
    nombre_mystere_ordinateur = rand()%1000+1;
    cout << "Saisissez ce que vous pensez etre le nombre mystere (entre 0 et 1000)"<<endl;
    cin >> nombre_utilisateur;
    while (nombre_utilisateur!= nombre_mystere_ordinateur){
                        if (nombre_utilisateur>nombre_mystere_ordinateur){
                                cout << "votre nombre est plus grand que le nombre mystère"<< endl;
                                     }
                        if (nombre_utilisateur<nombre_mystere_ordinateur){
                                cout << "votre nombre est plus petit que le nombre mystère"<< endl;
                                     }
                        else {
                                cout << "Bravo champion "<< endl;
                              }
                        compteur_essai_utlisateur=compteur_essai_utlisateur++;
                      }
   cout<< "bravo , pour trouver le nombre mystere vous avez fait "<< compteur_essai_utlisateur<< "essais"<< endl;
}
int main(int argc, char *argv[])
{
    Addition_Console();
    Inverse_Les_nombres_Saisis();
    Somme_de_pointeurs();
    Somme_De_Reference();
    Tri_Tableau_Rempli_Hasard();
    Choix_Taille_Tableau_Utilisateur();
    Remplissage_Aléatoire_Tableau(Taille_du_Tableau);/*Question */
    Afficher_Victoire_tennis_Console(score_joueur_un,score_joueur_deux);
    bonjour_nom_prenom_Console();
    bonjour_prenom_Console();
    Afficher_Nom_Prenom_Formater_Console();
    Deviner_nombre_mystere_ordinateur();
    nombre_mystere_essais();


    return 0;
}
