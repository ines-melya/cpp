#include "Joueur.h"

Joueur::Joueur()
{
}

Joueur::Joueur(const Couleurs& couleur) : m_couleur(couleur)
{
}

Joueur::~Joueur()
{
}

bool Joueur::EstBlanc()
{
    return this->m_couleur == Couleurs::BLANC ? true : false;
}

bool Joueur::EstNoir()
{
    return this->m_couleur == Couleurs::NOIR ? true : false;
}
