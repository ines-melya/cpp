#pragma once
#ifndef POSITION_H
#define POSITION_H

class Position
{
    public:
        Position();
        Position(const int&, const int&);
        ~Position();

        // Ajout de fonction directement dans le header qui se basent sur les fonctions creees dans le position.cpp
        inline int getLigne() const { return m_ligne; }
        inline void setLigne(int ligne) { this->m_ligne = ligne; }
        inline int getColonne() const { return m_colonne; }
        inline void setColonne(int colonne) { this->m_colonne = colonne; }

    private:
        int m_ligne;
        int m_colonne;
};

#endif // POSITION_H


