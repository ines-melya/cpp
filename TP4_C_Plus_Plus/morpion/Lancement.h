#pragma once
#ifndef LANCEMENT_H
#define LANCEMENT_H

#include <iostream>
#include <vector>
#include "Jeu.h"
#include "GrilleMorpion.h"
#include "Puissance4.h"
#include "Joueur.h"
#include <vector>
using namespace std;

// Classe qui gere le declanchement des differents jeux et associe les joueurs aux jeux qu ils auront choisi
class Lancement
{
public:
    Lancement();
    void Lancer();

private:
    void Relancer();
    char ChoixJeu();
    void Jouer(Jeu*);
    inline std::vector<Joueur> getJoueurs() const { return m_listeJoueurs; };
    void AjouterJoueur(const int&);
    Joueur ChangerJoueur(const Joueur&);
    vector<Joueur> m_listejoueurs;
};

#endif // LANCEMENT_H


