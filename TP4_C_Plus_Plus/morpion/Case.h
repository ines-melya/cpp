#pragma once
#ifndef CASE_H
#define CASE_H

#include "Position.h"
#include "Joueur.h"

enum class Types : int 
{
    VIDE = 1,
    PION = 2,
};

class Case
{
    public:
        Case();
        Case(const Position&, const Types&);
        Case(const Position&, const Types&, const Couleurs&);
        ~Case();
        // Ajout de fonction directement dans le header qui se basent sur les fonctions creees dans le case.cpp
        inline Position getPosition() const { return m_position; }
        inline void setPosition(const Position pos) { this->m_position = pos; }
        inline Types getType() const { return m_type; }
        inline void setType(const Types type) { this->m_type = type; }
        inline Couleurs getProprietaire() const { return m_proprietaire; }
        inline void setProprietaire(const Couleurs proprio) { this->m_proprietaire = proprio; }

        bool EstBlanc() const;
        bool EstNoir() const;
        bool EstVide() const;
        bool EstPion() const;
        void FaireVide();
        void FairePion();

    private:
        // Position de la case
        Position m_position;
        // Contenu de la case (role : X ou O)
        Types m_type;
        // Quel joueur dispose de quel role
        Couleurs m_proprietaire;
};

#endif // CASE_H


