#include "GrilleMorpion.h"

GrilleMorpion::GrilleMorpion(const Joueur& j1, const Joueur& j2) : m_joueur1(j1), m_joueur2(j2)
{
    compteur=0;
    AfficherRegles();
    initJeu();
}

GrilleMorpion::~GrilleMorpion()
{
}

// Affichage brut du tableau
void GrilleMorpion::initJeu()
{
    this->initPlateau();
    this->afficherPlateau();
}

void GrilleMorpion::initPlateau()
{
    int ligne,colonne;
    for (ligne = 0; ligne < nombre_lignes; ligne++)
    {
        for (colonne = 0; colonne < nombre_colonnes; colonne++)
        {
            this->m_grille[ligne][colonne] = Case(Position(ligne, colonne), Types::VIDE);
        }
    }
}
void GrilleMorpion::AfficherRegles()
{
    // Affichage des regles
    std::cout << "\n";
    std::cout << "Infos : La coordonnee x represente la ligne, se lit sur le cote de facon verticale de la grille" << std::endl;
    std::cout << "        La coordonnee y represente la colonne, se lit sur le haut de facon horizontale de la grille" << std::endl;
    std::cout << "        Les coordonnees sont comprises entre 1 et 3 inclus" << std::endl;
    std::cout << "\n";
}
void GrilleMorpion::afficherPlateau()
{
    int ligne,colonne;
    // Confectionner la grille de maniere visuelle dans la console
    for (ligne = 0; ligne < nombre_lignes; ligne++)
    {
        std::cout << "---------------------" << std::endl;
        for (colonne = 0; colonne < nombre_colonnes; colonne++)
        {
            std::cout << "| ";

            if (this->m_grille[ligne][colonne].isVide()){std::cout << "   ";}
            if (m_grille[ligne][colonne].isNoir()){std::cout << " X ";}
            if (m_grille[ligne][colonne].isBlanc()){std::cout << " O ";}

           }
            std::cout << " |";
        }
        std::cout << "\n---------------------" << std::endl;
    }
    std::cout << "\n";
}

void GrilleMorpion::faireTour(const Joueur& joueur)
{
    Position position = demanderPosition(joueur);
    this->deposerJeton(position, joueur);
    this->afficherPlateau();
}

Position GrilleMorpion::demanderPosition(const Joueur& joueur)
{
    int ligne = 0;
    int colonne = 0;

    // L utilisateur choisi les coordonnees qu il souhaite pour lea lignes et les colonnes
    while (true)
    {
        while (ligne > 3 || ligne < 1)
        {
            std::cout << "Entrez la coordonnee X de la case a remplir !" << std::endl;
            std::cin >> ligne;
        }

        while (colonne > 3 || colonne < 1)
        {
            std::cout << "Entrez la coordonnee Y de la case a remplir !" << std::endl;
            std::cin >> colonne;
        }

        ligne -= 1;
        colonne -= 1;

        if (this->m_grille[ligne][colonne].isVide())
        {
            Position position = Position(ligne, colonne);
            return position;
        }
        else
        {
            std::cout << "La case a deja ete initialisee par un joueur" << std::endl;
            std::cout << "\n";
            ligne = 0;
            colonne = 0;
        }
    }
}

void GrilleMorpion::deposerJeton(const Position& position, const Joueur& joueur)
{
    // Permet de positionner dans la case
    this->m_grille[position.getLigne()][position.getColonne()].FairePion();
    // Permet de positionner le role (X ou O) dans la case
    this->m_grille[position.getLigne()][position.getColonne()].setProprietaire(joueur.getCouleur());
}

bool GrilleMorpion::finJeu(const Joueur& joueur)
{
    // Permet la validation ou non du jeu
    if ((matchNul()) == true || (victoireJoueur(joueur)))
    {
        return true;
    }
    return false;
}

bool GrilleMorpion::victoireJoueur(const Joueur& joueur)
{
    // Si conditions de ligne, colonne, diagonale respectees, il y a victoire sur le jeu
    if (ligneComplete(joueur))
    {
        std::cout << "Le joueur a rempli une ligne complete" << std::endl;
        return true;
    }
    else if (colonneComplete(joueur))
    {
        std::cout << "Le joueur a rempli une colonne complete" << std::endl;
        return true;
    }
    else if (diagonaleComplete(joueur))
    {
        std::cout << "Le joueur a rempli une diagonale complete" << std::endl;
        return true;
    }
    else{return false;}
}
bool GrilleMorpion::WhatIsInCase()
{

}
// Test si aucun n a rempli les conditions pour gagner et que la grille est remplie et verification s il reste encore des case a remplir
bool GrilleMorpion::matchNul()
{
    int ligne,colonne;
    // On passe dans chaque lignes du tableau
    for (ligne = 0; ligne < nombre_lignes; ligne++)
    {
        // On passe dans chaque colonnes du tableau 
        for (colonne = 0; colonne < nombre_colonnes; colonne++)
        {
            if (this->m_grille[ligne][colonne].EstVide()){return false;}
        }
    }
    return true;
}

int GrilleMorpion::DecompteCaseComplete(const Joueur& joueur)
{
    int ligne,colonne;

    if (this->m_grille[ligne][colonne].getProprietaire() == joueur.getCouleur())
    {
        compteur++;
        if (compteur == complet){ return true;}
    }
    else {compteur=0;return false;}
}
// Fonction permettant la verification si condition du remplissage d une ligne est respectee 
bool GrilleMorpion::ligneComplete(const Joueur& joueur)
{
    int ligne,colonne;
    for (ligne = 0; ligne < nombre_lignes; ligne++)
    {
        for (colonne = 0; colonne < nombre_colonnes; colonne++)
        {
            DecompteCaseComplete(joueur);
        }
    }
}

// Fonction permettant la verification si condition du remplissage d une colonne est respectee 
bool GrilleMorpion::colonneComplete(const Joueur& joueur)
{
    int ligne,colonne;
    for (colonne = 0; colonne < nombre_lignes; colonne++)
    {
        for (ligne = 0; ligne < nombre_colonnes; ligne++)
        {
           DecompteCaseComplete(joueur);
        }
    }
    compteur=0;
    return false;
}

// Test des positions pour une diagonale sur une grille de 3 par 3 avec le mode morpion
bool GrilleMorpion::diagonaleComplete(const Joueur& joueur)
{
    if ((this->m_grille[0][0].getProprietaire() == joueur.getCouleur() &&
        this->m_grille[1][1].getProprietaire() == joueur.getCouleur() &&
        this->m_grille[2][2].getProprietaire() == joueur.getCouleur())
        ||
        (this->m_grille[0][2].getProprietaire() == joueur.getCouleur() &&
        this->m_grille[1][1].getProprietaire() == joueur.getCouleur() &&
        this->m_grille[2][0].getProprietaire() == joueur.getCouleur()))
    {
        return true;
    }
    else return false;
}
