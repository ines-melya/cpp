#include "Puissance4.h"

Puissance4::Puissance4(const Joueur& j1, const Joueur& j2) : m_joueur1(j1), m_joueur2(j2)
{
    initJeu();
}

Puissance4::~Puissance4()
{
}

// Affichage brut du tableau
void Puissance4::initJeu()
{
    initPlateau();
    afficherPlateau();
}

void Puissance4::initPlateau()
{
    int ligne;
    int colonne;

    for (ligne = 0; ligne < 6; ligne++)
    {
        for (colonne = 0; colonne < 7; colonne++)
        {
            this->m_grille[ligne][colonne] = Case(Position(ligne, colonne), Types::VIDE);
        }
    }
}

void Puissance4::afficherPlateau()
{
    int ligne;
    int colonne;

    // Affichage des regles
    std::cout << "\n";
    std::cout << "Infos : La coordonnee colonne se lit sur le haut de facon horizontale de la grille, vous avez donc & colonnes" << std::endl;
    std::cout << "But   : Il faut avoir au moins 4 cases remplies par un meme joueur dans une et meme colonne" << std::endl;
    std::cout << "        Les coordonnees sont comprises entre 1 et 7 inclus" << std::endl;
    std::cout << "\n";

    // Confectionner la grille de maniere visuelle dans la console
    std::cout << "\n";
    for (ligne = 0; ligne < 6; ligne++)
    {
        for (colonne = 0; colonne < 7; colonne++)
        {
            std::cout << "| ";
            if (this->m_grille[ligne][colonne].isVide())
            {
                std::cout << " ";
            }
            else
            {
                if (m_grille[ligne][colonne].isNoir())
                {
                    std::cout << "X";
                }
                else if (m_grille[ligne][colonne].isBlanc())
                {
                    std::cout << "O";
                }

            }
            std::cout << " |";
        }
        std::cout << "\n-----------------------------------" << std::endl;
    }
}


void Puissance4::faireTour(const Joueur& joueur)
{
    Position pos = demanderPosition(joueur);
    this->deposerJeton(pos, joueur);
    this->afficherPlateau();
}

Position Puissance4::demanderPosition(const Joueur& joueur)
{
    int colonne = 0;

    while (true)
    {
        // Contrairement au morpion l utilisateur choisi uniquement la colonne 
        while (colonne > 7 || colonne < 1)
        {
            std::cout << "Entrer la la colonne ou deposer le jeton !" << std::endl;
            std::cin >> colonne;
        }

        colonne -= 1;

        if (this->m_grille[0][colonne].isVide())
        {
            Position position = Position(0, colonne);
            return position;
        }
        else
        {
            std::cout << "La colonne est pleine" << std::endl;
            colonne = 0;
        }
    }
}

void Puissance4::deposerJeton(const Position& position, const Joueur& joueur)
{
    int ligneChute;
    for (ligneChute = 0; ligneChute < 6; ligneChute++)
    {
        if (!this->m_grille[ligneChute][position.getColonne()].isVide())
        {
            this->m_grille[ligneChute - 1][position.getColonne()].fairePion();
            this->m_grille[ligneChute - 1][position.getColonne()].setProprietaire(joueur.getCouleur());
            break;
        }

        if (ligneChute == 6 - 1)
        {
            this->m_grille[ligneChute][position.getColonne()].fairePion();
            this->m_grille[ligneChute][position.getColonne()].setProprietaire(joueur.getCouleur());
            break;
        }
    }
}

bool Puissance4::finJeu(const Joueur& joueur)
{
    // Permet la validation ou non du jeu
    if ((matchNul()) || (victoireJoueur(joueur)))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Puissance4::victoireJoueur(const Joueur& joueur)
{
    // Si conditions de ligne, colonne, diagonale respectees, il y a victoire sur le jeu
    if (victoireSurLigne(joueur))
    {
        std::cout << "Le joueur a aligne 4 pions de sa couleur en ligne" << std::endl;
        return true;
    }
    else if (victoireSurColonne(joueur))
    {
        std::cout << "Le joueur a aligne 4 pions de sa couleur en colonne" << std::endl;
        return true;
    }
    else if (victoireSurDiagonale(joueur))
    {
        std::cout << "Le joueur a aligne 4 pions de sa couleur en diagonale" << std::endl;
        return true;
    }
    else
    {
        return false;
    }
}

// Test si aucun n a rempli les conditions pour gagner et que la grille est remplie et verification s il reste encore des case a remplir
bool Puissance4::matchNul()
{
    int ligne;
    int colonne;

    for (ligne = 0; ligne < 6; ligne++)
    {
        for (colonne = 0; colonne < 7; colonne++)
        {
            if (this->m_grille[ligne][colonne].isVide())
            {
                return false;
            }
        }
    }
    return true;
}

// Fonction permettant la verification si condition du remplissage de 4 lignes dans une meme colonne est respectee 
bool Puissance4::victoireSurLigne(const Joueur& joueur) const
{
    int ligne;
    int colonne;
    int compteur = 0;

    for (ligne = 0; ligne < 6; ligne++)
    {
        for (colonne = 7 - 1; colonne >= 0; colonne--)
        {
            if (this->m_grille[ligne][colonne].getProprietaire() == joueur.getCouleur())
            {
                compteur++;

                if (compteur == 4)
                {
                    return true;
                }
            }
            else
            {
                compteur = 0;
            }
        }
        compteur = 0;
    }
    return false;
}

// Fonction permettant la verification si condition du remplissage d une colonne est respectee 
bool Puissance4::victoireSurColonne(const Joueur& joueur) const
{
    int ligne;
    int colonne;
    int compteur = 0;

    for (colonne = 0; colonne < 7; colonne++)
    {
        for (ligne = 6; ligne >= 0; ligne--)
        {
            if (this->m_grille[ligne][colonne].getProprietaire() == joueur.getCouleur())
            {
                compteur = compteur + 1;

                if (compteur == 4)
                {
                    return true;
                }
            }
            else
            {
                compteur = 0;
            }
        }
        compteur = 0;
    }
    return false;
}

bool Puissance4::victoireSurDiagonale(const Joueur& joueur) const
{
    int ligne;
    int colonne;
    int caseDiagonale;
    int compteur = 0;

    for (ligne = 0; ligne < 6; ligne++)
    {
        for (colonne = 0; colonne < 7; colonne++)
        {
            if (this->m_grille[ligne][colonne].getProprietaire() == joueur.getCouleur())
            {
                for (caseDiagonale = 0; caseDiagonale < 4; caseDiagonale++)
                {
                    // Test des diagonales dans le sens descendant
                    if (this->m_grille[ligne + caseDiagonale][colonne + caseDiagonale].getProprietaire() == this->m_grille[ligne][colonne].getProprietaire())
                    {
                        compteur = compteur + 1;

                        if (compteur == 4)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        compteur = 0;
                    }
                }

                for (caseDiagonale = 0; caseDiagonale < 4; caseDiagonale++)
                {
                    // Test des diagonales dans le sens ascendant
                    if (this->m_grille[ligne + caseDiagonale][colonne - caseDiagonale].getProprietaire() == this->m_grille[ligne][colonne].getProprietaire())
                    {
                        compteur = compteur + 1;

                        if (compteur == 4)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        compteur = 0;
                    }
                }
            }
        }
    }
    return false;
}
