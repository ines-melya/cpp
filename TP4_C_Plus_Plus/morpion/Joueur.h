#pragma once
#ifndef JOUEUR_H
#define JOUEUR_H

enum class Couleurs : int 
{
    NOIR = 1,
    BLANC = 2,
    AUCUNE = 0
};
// Classe qui permet d associer des couleurs aux joueurs
class Joueur
{
public:
    Joueur();
    Joueur(const Couleurs&);
    ~Joueur();
    // Ajout de fonction directement dans le header qui se basent sur les fonctions creees dans le joueur.cpp
    inline Couleurs getCouleur() const { return m_couleur; }
    inline void setCouleur(Couleurs couleur) { this->m_couleur = couleur; }

    bool EstBlanc();
    bool EstNoir();

private:
    Couleurs m_couleur;
};

#endif // JOUEUR_H


