#pragma once
#ifndef DAMES_H
#define DAMES_H

#include <iostream>
#include "Windows.h"
#include "Jeu.h"
#include "Case.h"

// Fonction pour avoir des couleurs aux textes dans la console : Dames -> Gris fonce = 8 et Pion -> Blanc = 7;
static HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

enum class Damier : int {
    CARRE_BLANC = 0xDB,
    CARRE_NOIR = 0xFF
};

// Dames est un enfant de la calsse Jeu
class Dames : public Jeu
{
    public:
        Dames(const Joueur&, const Joueur&, const int&, const int&);
        ~Dames() override;

        // Methodes pour rendre le deroulement du jeu dynamique et reutilisable
        void initJeu() override;
        void initPlateau() override;
        void faireTour(const Joueur&) override;
        Position demanderPosition(const Joueur&) override;
        bool finJeu(const Joueur&) override;
        bool matchNul() override;

        // Methodes suivies du TP 
        inline int getNbBlancs() { return m_nbBlancs; }
        inline void setNbBlancs(const int nb) { this->m_nbBlancs = nb; }
        inline int getNbNoirs() { return m_nbNoirs; }
        inline void setNbNoirs(const int nb) { this->m_nbNoirs = nb; }
        void deposerJeton(const Position&, const Joueur&) override;
        void afficherLigne(int, Damier, Damier);
        Position nouvelEmplacementCase(const Joueur&);
        void viderCase(const Position&);
        bool victoireJoueur(const Joueur&) override;
        void afficherPlateau() override;

    private:
        // Grille propre au mode de jeu Dames (8 lignes et colonnes)
        Case m_plateau[8][8];
        const Joueur& m_joueur1;
        const Joueur& m_joueur2;
        int m_nbBlancs;
        int m_nbNoirs;
};

#endif // DAMES_H