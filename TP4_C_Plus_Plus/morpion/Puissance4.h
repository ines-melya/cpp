#pragma once
#ifndef PUISSANCE4_H
#define PUISSANCE4_H

#include <iostream>
#include "Jeu.h"
#include "Case.h"

// Puissance4 est un enfant de la calsse Jeu
class Puissance4 : public Jeu
{
    public:
        Puissance4(const Joueur&, const Joueur&);
        ~Puissance4() override;

        // Methodes pour rendre le deroulement du jeu dynamique et reutilisable
        void initJeu() override;
        void initPlateau() override;
        void faireTour(const Joueur&) override;
        Position demanderPosition(const Joueur&) override;
        bool finJeu(const Joueur&) override;
        bool matchNul() override;

        // Methodes suivies du TP 
        void deposerJeton(const Position&, const Joueur&) override;
        bool victoireSurLigne(const Joueur&) const;
        bool victoireSurColonne(const Joueur&) const;
        bool victoireSurDiagonale(const Joueur&) const;
        bool victoireJoueur(const Joueur&) override;
        void afficherPlateau() override;

    private:
        // Nouvelle grille propre au mode de jeu Morpion version Puisssance 4
        Case m_grille[6][7];
        const Joueur& m_joueur1;
        const Joueur& m_joueur2;
};

#endif // GRILLEPUISSANCE4_H


