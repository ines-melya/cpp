#pragma once
#ifndef GRILLEMORPION_H
#define GRILLEMORPION_H

#include <iostream>
#include "Jeu.h"
#include "Case.h"

// GrilleMorpion est un enfant de la calsse Jeu
class GrilleMorpion : public Jeu
{
    public:
        GrilleMorpion(const Joueur&, const Joueur&);
        ~GrilleMorpion() override;

        // Methodes pour rendre le deroulement du jeu dynamique et reutilisable
        void initJeu() override;
        void initPlateau() override;
        void faireTour(const Joueur&) override;
        void AfficherRegles();
        bool WhatIsInCase();
        int DecompteCaseComplete(const Joueur&);
        Position demanderPosition(const Joueur&) override;
        bool finJeu(const Joueur&) override;
        bool matchNul() override; 

        // Methodes suivies du TP 
        void deposerJeton(const Position&, const Joueur&) override;
        bool ligneComplete(const Joueur&);
        bool colonneComplete(const Joueur&);
        bool diagonaleComplete(const Joueur&);
        bool victoireJoueur(const Joueur&) override;
        void afficherPlateau() override;

    private:
        // Grille propre au mode de jeu Morpion basique
        Case m_grille[3][3];
        int compteur;
        const int nombre_lignes=3;
        const int nombre_colonnes=3;
        const int complet=3;
        const Joueur& m_joueur1;
        const Joueur& m_joueur2;

};

#endif // GRILLEMORPION_H


