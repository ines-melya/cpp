#include "Case.h"

Case::Case()
{
}

Case::Case(const Position& pos, const Types& type) : m_position(pos), m_type(type)
{
}

Case::Case(const Position& pos, const Types& type, const Couleurs& proprio) : m_position(pos), m_type(type), m_proprietaire(proprio)
{
}

Case::~Case()
{
}

// Tests du role (X ou O) en fonction du joueur
bool Case::IsBlanc() const
{
    return this->m_proprietaire == Couleurs::BLANC ? true : false;
}

bool Case::IsNoir() const
{
    return this->m_proprietaire == Couleurs::NOIR ? true : false;
}

// Test si case est vide
bool Case::EstVide() const
{
    return this->m_type == Types::VIDE ? true : false;
}

void Case::FaireVide()
{
    this->m_type = Types::VIDE;
    this->m_proprietaire = Couleurs::AUCUNE;
}

// Test sur le Pion pour la partie en mode puissance 4
bool Case::EstPion() const
{
    return this->m_type == Types::PION ? true : false;
}

void Case::FairePion()
{
    this->m_type = Types::PION;
}
