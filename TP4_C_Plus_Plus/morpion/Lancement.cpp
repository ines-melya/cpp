#include "Lancement.h"

Lancement::Lancement()
{
}

char Lancement::ChoixJeu()
{
    char choixUser = 'a';

    // Permettre a l utilisateur de pouvoir choisir entre deux mode de jeux ou quitter le programme
    while ((choixUser != 'm') && (choixUser != 'p') && (choixUser != 'q'))
    {
        std::cout << "\nQuel jeu lancer ? Morpion : M, Puissance 4 : P, Quitter : Q \nSaisir la lettre correspondante puis appuyer sur entrer\n\n";
        std::cin >> choixUser;
    }
    return choixUser;
}

// Fonction qui permet d ajouter les joueurs, de leur associer une couleur, pour chaque jeu, ce en fonction du choix de l utilisateur 
void Lancement::Lancer()
{
    Jeu* jeuVar = NULL;

    switch (choixJeu())
    {
    case 'm':
        ajouterJoueur(2);
        m_listeJoueurs[0].setCouleur(Couleurs::NOIR);
        m_listeJoueurs[1].setCouleur(Couleurs::BLANC);
        jeuVar = new GrilleMorpion(m_listeJoueurs[0], m_listeJoueurs[1]);
        break;
    case 'p':
        ajouterJoueur(2);
        m_listeJoueurs[0].setCouleur(Couleurs::NOIR);
        m_listeJoueurs[1].setCouleur(Couleurs::BLANC);
        jeuVar = new Puissance4(m_listeJoueurs[0], m_listeJoueurs[1]);
        break;
    case 'q':
        // Sortir du programme lorsque l utilisateur saisi les conditions pour quitter
        std::cout << "\nPartie terminee, au revoir\n" << std::endl;
        exit(0);
        break;
    default:
        break;
    }

    jouer(jeuVar);
    this->Relancer();
}

// Fonction aui permet de relancer le jeu
void Lancement::Relancer()
{
    Lancer();
}

void Lancement::Jouer(Jeu* jeuVar)
{
    Joueur joueur_actuel = m_listeJoueurs[0];

    while (true)
    {
        jeuVar->faireTour(joueurActuel);

        if (jeuVar->finJeu(joueurActuel))
        {
            std::cout << "\nFin de la partie\n" << std::endl;
            delete jeuVar;
            break;
        }
        joueur_actuel = changerJoueur(joueurActuel);
    }
}

void Lancement::AjouterJoueur(const int& nouveaux_joueurs)
{
    int unJoueur;
    for (unJoueur = 0; unJoueur < nouveaux_joueurs; unJoueur++)
    {
        this->m_listeJoueurs.push_back(Joueur());
    }
}

Joueur Lancement::ChangerJoueur(const Joueur& joueur_actuel)
{
    Joueur nouveau_joueur;
    unsigned int index = 0;

    for (auto iter = m_listeJoueurs.begin(); iter != m_listeJoueurs.end(); ++iter)
    {
        if ((iter->getCouleur() == joueur_actuel.getCouleur()) && (index != m_listeJoueurs.size() - 1))
        {
            nouveau_joueur = m_listeJoueurs[index + 1];
            return nouveau_joueur;
        }
        index++;
    }
    nouveau_joueur = m_listeJoueurs[0];
    return nouveau_joueur;
}
