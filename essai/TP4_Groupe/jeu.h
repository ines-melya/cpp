#pragma once
#ifndef JEU_H
#define JEU_H

#include <iostream>
#include <string>
#include "position.h"
#include "joueur.h"
#include "case.h"

class Jeu
{
    /*
    *   Polymorphisme avec l'utilisation du mot cle 'virtual' pour la classe mere
    *   On declare virtuelle la fonction de la classe de base (Jeu.h) qui est elle est redefinie dans les classes filles (GrilleMirpion.h et Puissance4.h)
    */

    public:
        Jeu();
        virtual ~Jeu();
        virtual void InitJeu();
        virtual void InitPlateau();
        virtual void AfficherPlateau();
        virtual Position DemanderPosition(const Joueur&);
        virtual void DeposerJeton(const Position&, const Joueur&);
        virtual void FaireTour(const Joueur&);
        virtual bool MatchNul();
        virtual bool VictoireJoueur(const Joueur&);
        virtual bool FinJeu(const Joueur&);
};

#endif // JEU_H

