#include "jeu.h"

Jeu::Jeu()
{
}

Jeu::~Jeu()
{
}

void Jeu::InitJeu()
{
}

void Jeu::InitPlateau()
{
}

void Jeu::AfficherPlateau()
{
}

Position Jeu::DemanderPosition(const Joueur&)
{
    return Position();
}

void Jeu::DeposerJeton(const Position&, const Joueur&)
{
}

bool Jeu::VictoireJoueur(const Joueur&)
{
    return false;
}

void Jeu::FaireTour(const Joueur&)
{
}

bool Jeu::MatchNul()
{
    return false;
}

bool Jeu::FinJeu(const Joueur&)
{
    return false;
}
