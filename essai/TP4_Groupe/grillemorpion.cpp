#include "grillemorpion.h"

GrilleMorpion::GrilleMorpion(const Joueur& j1, const Joueur& j2) : m_joueur1(j1), m_joueur2(j2)
{
    AfficherRegles();
    InitJeu();
}

GrilleMorpion::~GrilleMorpion()
{
}

// Affichage brut du tableau
void GrilleMorpion::InitJeu()
{
    this->InitPlateau();
    this->AfficherPlateau();
}

void GrilleMorpion::InitPlateau()
{
    int ligne,colonne;
    for (ligne = 0; ligne < m_nombre_lignes; ligne++)
    {
        for (colonne = 0; colonne < m_nombre_colonnes; colonne++)
        {
            this->m_grille[ligne][colonne] = Case(Position(ligne, colonne), Types::VIDE);
        }
    }
}
void GrilleMorpion::AfficherRegles()
{
    // Affichage des regles
    std::cout << "\n";
    std::cout << "Infos : La coordonnee x represente la ligne, se lit sur le cote de facon verticale de la grille" << std::endl;
    std::cout << "        La coordonnee y represente la colonne, se lit sur le haut de facon horizontale de la grille" << std::endl;
    std::cout << "        Les coordonnees sont comprises entre 1 et 3 inclus" << std::endl;
    std::cout << "\n";
}
void GrilleMorpion::AfficherPlateau()
{
    int ligne,colonne;
    // Confectionner la grille de maniere visuelle dans la console
    for (ligne = 0; ligne < m_nombre_lignes; ligne++)
    {
        std::cout << "---------------------" << std::endl;
        for (colonne = 0; colonne < m_nombre_colonnes; colonne++)
        {
            std::cout << "| ";

            if (this->m_grille[ligne][colonne].EstVide()){std::cout << "   ";}
            if (m_grille[ligne][colonne].EstNoir()){std::cout << " X ";}
            if (m_grille[ligne][colonne].EstBlanc()){std::cout << " O ";}

           }
            std::cout << " |";
        }
        std::cout << "\n---------------------" << std::endl;
    }
    /*
    std::cout << "\n";*/


void GrilleMorpion::FaireTour(const Joueur& joueur)
{
    Position position = DemanderPosition(joueur);
    this->DeposerJeton(position, joueur);
    this->AfficherPlateau();
}

Position GrilleMorpion::DemanderPosition(const Joueur& joueur)
{
    int ligne = 0;
    int colonne = 0;

    // L utilisateur choisi les coordonnees qu il souhaite pour lea lignes et les colonnes
    while (true)
    {
        while (ligne > 3 || ligne < 1)
        {
            std::cout << "Entrez la coordonnee X de la case a remplir !" << std::endl;
            std::cin >> ligne;
        }

        while (colonne > 3 || colonne < 1)
        {
            std::cout << "Entrez la coordonnee Y de la case a remplir !" << std::endl;
            std::cin >> colonne;
        }

        ligne -= 1;
        colonne -= 1;

        if (this->m_grille[ligne][colonne].EstVide())
        {
            Position position = Position(ligne, colonne);
            return position;
        }
        else
        {
            std::cout << "La case a deja ete initialisee par un joueur" << std::endl;
            std::cout << "\n";
            ligne = 0;
            colonne = 0;
        }
    }
}

void GrilleMorpion::DeposerJeton(const Position& position, const Joueur& joueur)
{
    // Permet de positionner dans la case
    this->m_grille[position.getLigne()][position.getColonne()].CreerPion();
    // Permet de positionner le role (X ou O) dans la case
    this->m_grille[position.getLigne()][position.getColonne()].setProprietaire(joueur.getCouleur());
}

bool GrilleMorpion::FinJeu(const Joueur& joueur)
{
    // Permet la validation ou non du jeu
    if ((MatchNul()) == true || (VictoireJoueur(joueur)))
    {
        return true;
    }
    return false;
}

bool GrilleMorpion::VictoireJoueur(const Joueur& joueur)
{
    // Si conditions de ligne, colonne, diagonale respectees, il y a victoire sur le jeu
    if (LigneComplete(joueur))
    {
        std::cout << "Le joueur a rempli une ligne complete" << std::endl;
        return true;
    }
    else if (ColonneComplete(joueur))
    {
        std::cout << "Le joueur a rempli une colonne complete" << std::endl;
        return true;
    }
    else if (DiagonaleComplete(joueur))
    {
        std::cout << "Le joueur a rempli une diagonale complete" << std::endl;
        return true;
    }
    else{return false;}
}

// Test si aucun n a rempli les conditions pour gagner et que la grille est remplie et verification s il reste encore des case a remplir
bool GrilleMorpion::MatchNul()
{
    int ligne,colonne;
    // On passe dans chaque lignes du tableau
    for (ligne = 0; ligne < m_nombre_lignes; ligne++)
    {
        // On passe dans chaque colonnes du tableau
        for (colonne = 0; colonne < m_nombre_colonnes; colonne++)
        {
            if (this->m_grille[ligne][colonne].EstVide()){return false;}
        }
    }
    return true;
}

// Fonction permettant la verification si condition du remplissage d une ligne est respectee
bool GrilleMorpion::LigneComplete(const Joueur& joueur)
{
    int ligne,colonne;
    for (ligne = 0; ligne < m_nombre_lignes; ligne++)
    {
        for (colonne = 0; colonne < m_nombre_colonnes; colonne++)
        {
            if( m_grille[ligne][colonne].getProprietaire()!= joueur.getCouleur()){return false;}
        }
        return true;
    }
}

// Fonction permettant la verification si condition du remplissage d une colonne est respectee
bool GrilleMorpion::ColonneComplete(const Joueur& joueur)
{
    int ligne,colonne;
    for (colonne = 0; colonne < m_nombre_lignes; colonne++)
    {
        for (ligne = 0; ligne < m_nombre_colonnes; ligne++)
        {
            if( m_grille[ligne][colonne].getProprietaire()!= joueur.getCouleur()){return false;}
        }
    return true ;
    }
}

// Test des positions pour une diagonale sur une grille de 3 par 3 avec le mode morpion
bool GrilleMorpion::DiagonaleComplete(const Joueur& joueur)
{
    if ((this->m_grille[0][0].getProprietaire() == joueur.getCouleur() &&
        this->m_grille[1][1].getProprietaire() == joueur.getCouleur() &&
        this->m_grille[2][2].getProprietaire() == joueur.getCouleur())
        ||
        (this->m_grille[0][2].getProprietaire() == joueur.getCouleur() &&
        this->m_grille[1][1].getProprietaire() == joueur.getCouleur() &&
        this->m_grille[2][0].getProprietaire() == joueur.getCouleur()))
    {
        return true;
    }
    else return false;
}
