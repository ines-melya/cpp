#pragma once
#ifndef LANCEMENT_H
#define LANCEMENT_H

#include <iostream>
#include <vector>
#include "jeu.h"
#include "grillemorpion.h"
#include "puissance4.h"
#include "joueur.h"
#include "dames.h"
#include <vector>
using namespace std;

// Classe qui gere le declanchement des differents jeux et associe les joueurs aux jeux qu ils auront choisi
class Lancement
{
public:
    Lancement();
    void Lancer();

private:
    void Relancer();
    char ChoixJeu();
    void Jouer(Jeu*);
    inline std::vector<Joueur> getJoueurs() const { return m_listejoueurs; };
    void AjouterJoueur(const int&);
    Joueur ChangerJoueur(const Joueur&);
    vector<Joueur> m_listejoueurs;
};

#endif // LANCEMENT_H
