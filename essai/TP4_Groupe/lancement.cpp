#include "lancement.h"

Lancement::Lancement()
{
}

char Lancement::ChoixJeu()
{
    char choixUser = 'a';

    // Permettre a l utilisateur de pouvoir choisir entre deux mode de jeux ou quitter le programme
    while ((choixUser != 'm') && (choixUser != 'p') && (choixUser != 'd') && (choixUser != 'q'))
    {
        std::cout << "\nQuel jeu lancer ? Morpion : M, Puissance 4 : P, Dames : D, Quitter : Q \nSaisir la lettre correspondante puis appuyer sur entrer\n\n";
        std::cin >> choixUser;
    }
    return choixUser;
}

// Fonction qui permet d ajouter les joueurs, de leur associer une couleur, pour chaque jeu, ce en fonction du choix de l utilisateur
void Lancement::Lancer()
{
    Jeu* jeuVar = NULL;

    switch (ChoixJeu())
    {
    case 'm':
        AjouterJoueur(2);
        m_listejoueurs[0].setCouleur(Couleurs::NOIR);
        m_listejoueurs[1].setCouleur(Couleurs::BLANC);
        jeuVar = new GrilleMorpion(m_listejoueurs[0], m_listejoueurs[1]);
        break;
    case 'p':
        AjouterJoueur(2);
        m_listejoueurs[0].setCouleur(Couleurs::NOIR);
        m_listejoueurs[1].setCouleur(Couleurs::BLANC);
        jeuVar = new Puissance4(m_listejoueurs[0], m_listejoueurs[1]);
        break;
    case 'd':
        AjouterJoueur(2);
        m_listejoueurs[0].setCouleur(Couleurs::NOIR);
        m_listejoueurs[1].setCouleur(Couleurs::BLANC);
        jeuVar = new Dames(m_listejoueurs[0], m_listejoueurs[1], 12, 12);
        break;
    case 'q':
        // Sortir du programme lorsque l utilisateur saisi les conditions pour quitter
        std::cout << "\nPartie terminee, au revoir\n" << std::endl;
        exit(0);
        break;
    default:
        break;
    }

    Jouer(jeuVar);
    this->Relancer();
}

// Fonction aui permet de relancer le jeu
void Lancement::Relancer()
{
    Lancer();
}

void Lancement::Jouer(Jeu* jeuVar)
{
    Joueur joueur_actuel = m_listejoueurs[0];

    while (true)
    {
        jeuVar->FaireTour(joueur_actuel);

        if (jeuVar->FinJeu(joueur_actuel))
        {
            std::cout << "\nFin de la partie\n" << std::endl;
            delete jeuVar;
            break;
        }
        joueur_actuel = ChangerJoueur(joueur_actuel);
    }
}

void Lancement::AjouterJoueur(const int& nouveaux_joueurs)
{
    int unJoueur;
    for (unJoueur = 0; unJoueur < nouveaux_joueurs; unJoueur++)
    {
        this->m_listejoueurs.push_back(Joueur());
    }
}

Joueur Lancement::ChangerJoueur(const Joueur& joueur_actuel)
{
    Joueur nouveau_joueur;
    unsigned int index = 0;

    for (auto iter = m_listejoueurs.begin(); iter != m_listejoueurs.end(); ++iter)
    {
        if ((iter->getCouleur() == joueur_actuel.getCouleur()) && (index != m_listejoueurs.size() - 1))
        {
            nouveau_joueur = m_listejoueurs[index + 1];
            return nouveau_joueur;
        }
        index++;
    }
    nouveau_joueur = m_listejoueurs[0];
    return nouveau_joueur;
}
