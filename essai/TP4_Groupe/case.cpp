#include "case.h"

Case::Case()
{
}

Case::Case(const Position& pos, const Types& type) : m_position(pos), m_type(type)
{
}

Case::Case(const Position& pos, const Types& type, const Couleurs& proprio) : m_position(pos), m_type(type), m_proprietaire(proprio)
{
}

Case::~Case()
{
}

// Tests du role (X ou O) en fonction du joueur
bool Case::EstBlanc() const
{
    return this->m_proprietaire == Couleurs::BLANC ? true : false;
}

bool Case::EstNoir() const
{
    return this->m_proprietaire == Couleurs::NOIR ? true : false;
}

// Test si case est vide
bool Case::EstVide() const
{
    return this->m_type == Types::VIDE ? true : false;
}

void Case::ViderCase()
{
    this->m_type = Types::VIDE;
    this->m_proprietaire = Couleurs::AUCUNE;
}

// Test sur le Pion pour la partie en mode puissance 4
bool Case::EstPion() const
{
    return this->m_type == Types::PION ? true : false;
}

void Case::CreerPion()
{
    this->m_type = Types::PION;
}
bool Case::EstDame() const
{
    return this->m_type == Types::DAME ? true : false;
}

void Case::CreerDame()
{
    this->m_type = Types::DAME;
}
