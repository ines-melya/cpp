#pragma once
#ifndef GRILLEMORPION_H
#define GRILLEMORPION_H

#include <iostream>
#include "jeu.h"
#include "case.h"

// GrilleMorpion est un enfant de la calsse Jeu
class GrilleMorpion : public Jeu
{
    public:
        GrilleMorpion(const Joueur&, const Joueur&);
        ~GrilleMorpion() override;

        // Methodes pour rendre le deroulement du jeu dynamique et reutilisable
        void InitJeu() override;
        void InitPlateau() override;
        void FaireTour(const Joueur&) override;
        void AfficherRegles();
        int DecompteCasesCompletes(int ligne , int colonne, const Joueur&,int compteur);
        Position DemanderPosition(const Joueur&) override;
        bool FinJeu(const Joueur&) override;
        bool MatchNul() override;

        // Methodes suivies du TP
        void DeposerJeton(const Position&, const Joueur&) override;
        bool LigneComplete(const Joueur&);
        bool ColonneComplete(const Joueur&);
        bool DiagonaleComplete(const Joueur&);
        bool VictoireJoueur(const Joueur&) override;
        void AfficherPlateau() override;

    private:
        // Grille propre au mode de jeu Morpion basique
        Case m_grille[3][3];
        const int m_nombre_lignes=3;
        const int m_nombre_colonnes=3;
        const int m_complet=3;
        const Joueur& m_joueur1;
        const Joueur& m_joueur2;

};

#endif // GRILLEMORPION_H

