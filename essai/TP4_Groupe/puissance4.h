#pragma once
#ifndef PUISSANCE4_H
#define PUISSANCE4_H

#include <iostream>
#include "jeu.h"
#include "case.h"

// Puissance4 est un enfant de la classe Jeu
class Puissance4 : public Jeu
{
    public:
        Puissance4(const Joueur&, const Joueur&);
        ~Puissance4() override;

        // Methodes pour rendre le deroulement du jeu dynamique et reutilisable
        void InitJeu() override;
        void InitPlateau() override;
        void FaireTour(const Joueur&) override;
        Position DemanderPosition(const Joueur&) override;
        bool FinJeu(const Joueur&) override;
        bool MatchNul() override;

        // Methodes suivies du TP
        void DeposerJeton(const Position&, const Joueur&) override;
        bool VictoireSurLigne(const Joueur&) const;
        bool VictoireSurColonne(const Joueur&) const;
        bool VictoireSurDiagonaleAscendant(const Joueur&) const;
        bool VictoireSurDiagonaleDescendant(const Joueur&) const;
        bool VictoireJoueur(const Joueur&) override;
        void AfficherPlateau() override;
        void AfficherRegles();
        int DecompteCaseComplete(int ligne , int colonne, const Joueur&,int compteur)const;

    private:
        // Nouvelle grille propre au mode de jeu Morpion version Puisssance 4
        Case m_grille[6][7];
        const int m_nombre_colonne=7;
        const int m_nombre_lignes=6;
        const int m_complet=4;
        const Joueur& m_joueur1;
        const Joueur& m_joueur2;
};

#endif // GRILLEPUISSANCE4_H


