#include "puissance4.h"

Puissance4::Puissance4(const Joueur& j1, const Joueur& j2) : m_joueur1(j1), m_joueur2(j2)
{
    AfficherRegles();
    InitJeu();
}

Puissance4::~Puissance4()
{
}

// Affichage brut du tableau
void Puissance4::InitJeu()
{
    InitPlateau();
    AfficherPlateau();
}

void Puissance4::InitPlateau()
{
    int ligne,colonne;

    for (ligne = 0; ligne < m_nombre_lignes; ligne++)
    {
        for (colonne = 0; colonne < m_nombre_colonne; colonne++)
        {
            this->m_grille[ligne][colonne] = Case(Position(ligne, colonne), Types::VIDE);
        }
    }
}
void Puissance4::AfficherRegles()
{
    // Affichage des regles
    std::cout << "\n";
    std::cout << "Infos : La coordonnee colonne se lit sur le haut de facon horizontale de la grille, vous avez donc & colonnes" << std::endl;
    std::cout << "But   : Il faut avoir au moins 4 cases remplies par un meme joueur dans une et meme colonne" << std::endl;
    std::cout << "        Les coordonnees sont comprises entre 1 et 7 inclus" << std::endl;
    std::cout << "\n";
}
void Puissance4::AfficherPlateau()
{
    int ligne,colonne;



    std::cout << "\n";
    for (ligne = 0; ligne < 6; ligne++)
    {
        for (colonne = 0; colonne < 7; colonne++)
        {
            std::cout << "| ";
            if (this->m_grille[ligne][colonne].EstVide())
            {
                std::cout << " ";
            }
            else
            {
                if (m_grille[ligne][colonne].EstNoir())
                {
                    std::cout << "X";
                }
                else if (m_grille[ligne][colonne].EstBlanc())
                {
                    std::cout << "O";
                }

            }
            std::cout << " |";
        }
        std::cout << "\n-----------------------------------" << std::endl;
    }
}


void Puissance4::FaireTour(const Joueur& joueur)
{
    Position pos = DemanderPosition(joueur);
    this->DeposerJeton(pos, joueur);
    this->AfficherPlateau();
}

Position Puissance4::DemanderPosition(const Joueur& joueur)
{
    int colonne = 0;

    while (true)
    {
        // Contrairement au morpion l utilisateur choisi uniquement la colonne
        while (colonne > 7 || colonne < 1)
        {
            std::cout << "Entrer la la colonne ou deposer le jeton !" << std::endl;
            std::cin >> colonne;
        }

        colonne -= 1;

        if (this->m_grille[0][colonne].EstVide())
        {
            Position position = Position(0, colonne);
            return position;
        }
        else
        {
            std::cout << "La colonne est pleine" << std::endl;
            colonne = 0;
        }
    }
}

void Puissance4::DeposerJeton(const Position& position, const Joueur& joueur)
{
    int ligneChute;
    for (ligneChute = 0; ligneChute < 6; ligneChute++)
    {
        if (!this->m_grille[ligneChute][position.getColonne()].EstVide())
        {
            this->m_grille[ligneChute - 1][position.getColonne()].CreerPion();
            this->m_grille[ligneChute - 1][position.getColonne()].setProprietaire(joueur.getCouleur());
            break;
        }

        if (ligneChute == 6 - 1)
        {
            this->m_grille[ligneChute][position.getColonne()].CreerPion();
            this->m_grille[ligneChute][position.getColonne()].setProprietaire(joueur.getCouleur());
            break;
        }
    }
}

bool Puissance4::FinJeu(const Joueur& joueur)
{
    // Permet la validation ou non du jeu
    if ((MatchNul()) || (VictoireJoueur(joueur)))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Puissance4::VictoireJoueur(const Joueur& joueur)
{
    // Si conditions de ligne, colonne, diagonale respectees, il y a victoire sur le jeu
    if (VictoireSurLigne(joueur))
    {
        std::cout << "Le joueur a aligne 4 pions de sa couleur en ligne" << std::endl;
        return true;
    }
    else if (VictoireSurColonne(joueur))
    {
        std::cout << "Le joueur a aligne 4 pions de sa couleur en colonne" << std::endl;
        return true;
    }
    else if (VictoireSurDiagonaleAscendant(joueur)or VictoireSurDiagonaleDescendant(joueur))
    {
        std::cout << "Le joueur a aligne 4 pions de sa couleur en diagonale" << std::endl;
        return true;
    }
    else
    {
        return false;
    }
}

// Test si aucun n a rempli les conditions pour gagner et que la grille est remplie et verification s il reste encore des case a remplir
bool Puissance4::MatchNul()
{
    int ligne,colonne;

    for (ligne = 0; ligne < m_nombre_lignes; ligne++)
    {
        for (colonne = 0; colonne < m_nombre_colonne; colonne++)
        {
            if (this->m_grille[ligne][colonne].EstVide())
            {
                return false;
            }
        }
    }
    return true;
}

// Fonction permettant la verification si condition du remplissage de 4 lignes dans une meme colonne est respectee
bool Puissance4::VictoireSurLigne(const Joueur& joueur) const
{
    int ligne,colonne;
    int compteur = 0;

    for (ligne = 0; ligne < m_nombre_lignes; ligne++)
    {
        for (colonne = 7 - 1; colonne >= 0; colonne--)
        {
            compteur=DecompteCaseComplete(ligne,colonne,joueur,compteur);
            if (compteur==m_complet){return true;}
        }
        compteur = 0;
    }
    return false;
}

int Puissance4::DecompteCaseComplete(int ligne, int colonne, const Joueur & joueur, int compteur) const
{
    if (this->m_grille[ligne][colonne].getProprietaire() == joueur.getCouleur())
    {
        compteur++;
    }
    else {compteur=0;}
    return compteur;

}
// Fonction permettant la verification si condition du remplissage d une colonne est respectee
bool Puissance4::VictoireSurColonne(const Joueur& joueur) const
{
    int ligne,colonne;
    int compteur = 0;

    for (colonne = 0; colonne < m_nombre_colonne; colonne++)
    {
        for (ligne = m_nombre_lignes; ligne >= 0; ligne--)
        {
            compteur=DecompteCaseComplete(ligne,colonne,joueur,compteur);
            if (compteur==m_complet){return true;}
        }
        compteur = 0;
    }
    return false;
}
bool Puissance4::VictoireSurDiagonaleDescendant(const Joueur & joueur) const
{

    int ligne,colonne,case_diagonale;
    int compteur = 0;

    for (ligne = 0; ligne < m_nombre_lignes; ligne++)
    {
        for (colonne = 0; colonne < m_nombre_colonne; colonne++)
        {
    for (case_diagonale = 0; case_diagonale < 4; case_diagonale++)
    {
        // Test des diagonales dans le sens descendant
        if (this->m_grille[ligne + case_diagonale][colonne + case_diagonale].getProprietaire() == this->m_grille[ligne][colonne].getProprietaire())
        {
            compteur=DecompteCaseComplete(ligne,colonne,joueur,compteur);
            if (compteur==m_complet){return true;}
        }
        compteur = 0;
    }
    return false;
    }
}}

bool Puissance4::VictoireSurDiagonaleAscendant(const Joueur & joueur) const
{
    int ligne,colonne,case_diagonale;
    int compteur = 0;

    for (ligne = 0; ligne < m_nombre_lignes; ligne++)
    {
        for (colonne = 0; colonne < m_nombre_colonne; colonne++)
        {
            if (this->m_grille[ligne][colonne].getProprietaire() == joueur.getCouleur())
            {
                for (case_diagonale = 0; case_diagonale < m_complet; case_diagonale++)
                {
                    // Test des diagonales dans le sens ascendant
                    if (this->m_grille[ligne + case_diagonale][colonne - case_diagonale].getProprietaire() == this->m_grille[ligne][colonne].getProprietaire())
                    {
                        compteur=DecompteCaseComplete(ligne,colonne,joueur,compteur);
                        if (compteur==m_complet){return true;}
                    }
                    compteur = 0;
                }
                return false;

            }
        }
    }

}
