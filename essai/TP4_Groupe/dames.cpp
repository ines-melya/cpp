#include "dames.h"
Dames::Dames(const Joueur& j1, const Joueur& j2, const int& nbBlancs, const int& nbNoirs) :
    m_joueur1(j1), m_joueur2(j2), m_nbBlancs(nbBlancs), m_nbNoirs(nbNoirs)
{
    InitJeu();
}

Dames::~Dames()
{
}

void Dames::InitJeu()
{
    this->InitPlateau();
    this->AfficherPlateau();
}

void Dames::InitPlateau()
{
    for (int ligne = 0; ligne < 8; ligne++)
    {
        for (int colonne = 0; colonne < 8; colonne++)
        {
            if (ligne == 0 && colonne % 2 == 0)
            {
                this->m_plateau[ligne][colonne] = Case(Position(ligne, colonne), Types::PION, Couleurs::BLANC);
            }

            else if (ligne == 1 && colonne % 2 != 0)
            {
                this->m_plateau[ligne][colonne] = Case(Position(ligne, colonne), Types::PION, Couleurs::BLANC);
            }

            else if (ligne == 2 && colonne % 2 == 0)
            {
                this->m_plateau[ligne][colonne] = Case(Position(ligne, colonne), Types::PION, Couleurs::BLANC);
            }

            else if (ligne == 5 && colonne % 2 != 0)
            {
                this->m_plateau[ligne][colonne] = Case(Position(ligne, colonne), Types::PION, Couleurs::NOIR);
            }

            else if (ligne == 6 && colonne % 2 == 0)
            {
                this->m_plateau[ligne][colonne] = Case(Position(ligne, colonne), Types::PION, Couleurs::NOIR);
            }

            else if (ligne == 7 && colonne % 2 != 0)
            {
                this->m_plateau[ligne][colonne] = Case(Position(ligne, colonne), Types::PION, Couleurs::NOIR);
            }
            else
            {
                this->m_plateau[ligne][colonne] = Case(Position(ligne, colonne), Types::VIDE);
            }
        }
    }
}

void Dames::AfficherPlateau()
{
    std::cout << "   1     2     3     4     5     6     7     8\n\n";

    for (int ligne = 0; ligne < 8; ligne++)
    {
        if (ligne % 2 == 0)
        {
            // ligne commençant par noir
            AfficherLigne(ligne, Damier::CARRE_NOIR, Damier::CARRE_BLANC);
        }
        else
        {
            // ligne commençant par blanc
            AfficherLigne(ligne, Damier::CARRE_BLANC, Damier::CARRE_NOIR);
        }
    }
    std::cout << "\n";
}

void Dames::FaireTour(const Joueur& joueur)
{
    Position posDepart = DemanderPosition(joueur);
    Position posDeplacement = nouvelEmplacementCase(joueur);

    this->DeposerJeton(posDeplacement, joueur);
    this->ViderCase(posDepart);
    this->AfficherPlateau();
}

// Apres avoir selectionne la piece que l on veut deplacer, il faut selectionner la nouvelle case dans laquelle la piece sera
Position Dames::DemanderPosition(const Joueur& joueur)
{
    int ligne = 0;
    int colonne = 0;

    while (true)
    {
        while (ligne > 8 || ligne < 1)
        {
            std::cout << "Pour selectionner une piece entrez un numero de ligne\n" << std::endl;
            std::cin >> ligne;
        }

        while (colonne > 8 || colonne < 1)
        {
            std::cout << "Pour selectionner une piece entrez un numero de colonne\n" << std::endl;
            std::cin >> colonne;
        }

        ligne -= 1;
        colonne -= 1;

        if (this->m_plateau[ligne][colonne].getProprietaire() == joueur.getCouleur())
        {
            Position position = Position(ligne, colonne);
            return position;
        }
        else if (this->m_plateau[ligne][colonne].EstVide())
        {
            std::cout << "Cette case ne comporte pas de jeton.\n";
            ligne = 0;
            colonne = 0;
        }
        else
        {

            std::cout << "Cette piece est a l'autre joueur.\n";
            ligne = 0;
            colonne = 0;
        }
    }
}

void Dames::DeposerJeton(const Position& position, const Joueur& joueur)
{
    this->m_plateau[position.getLigne()][position.getColonne()].setType(Types::PION);
    this->m_plateau[position.getLigne()][position.getColonne()].setProprietaire(joueur.getCouleur());
}

bool Dames::FinJeu(const Joueur& joueur)
{
    if ((MatchNul()) || (VictoireJoueur(joueur)))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Dames::VictoireJoueur(const Joueur& joueur)
{
    if ((this->getNbNoirs() == 0) || (this->getNbBlancs() == 0))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Dames::MatchNul()
{
    char choixNoir;
    char choixBlanc;
    std::cout << "Joueur noir, voulez vous un match Nul ? Si oui tapez 'O', si non tapez 'N'.\n";
    std::cin >> choixNoir;

    std::cout << "Joueur blanc, voulez vous un match Nul ? Si oui tapez 'O', si non tapez 'N'.\n";
    std::cin >> choixBlanc;

    if (((choixNoir == 'o') && (choixBlanc == 'o')) || ((choixNoir == 'O') && (choixBlanc == 'O')))
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Dames::AfficherLigne(int ligne, Damier couleur1, Damier couleur2)
{
    // Total pour faire un carré avec vertical soit : CASE/2
    const int CASE = 6;

    for (int interligne = 0; interligne < CASE / 2; interligne++)
    {
        // Interligne = 8 cases groupées en 4 paires noire+blanche
        for (int paire = 0; paire < 4; paire++)
        {
            // Première case de la paire
            for (int subColonne = 0; subColonne < CASE; subColonne++)
            {
                if (interligne == 1 && subColonne == 3)
                {
                    if (this->m_plateau[ligne][paire * 2].EstNoir())
                    {
                        if (m_plateau[ligne][paire * 2].EstDame())
                        {
                            SetConsoleTextAttribute(hConsole, 8); // Couleur gris fonce
                            std::cout << 'X';
                            SetConsoleTextAttribute(hConsole, 7); // Couleur blanche
                        }
                        else
                        {
                            std::cout << 'X';
                        }
                    }

                    if (this->m_plateau[ligne][paire * 2].EstBlanc())
                    {
                        if (m_plateau[ligne][paire * 2].EstDame())
                        {
                            SetConsoleTextAttribute(hConsole, 8); // Couleur gris fonce
                            std::cout << 'O';
                            SetConsoleTextAttribute(hConsole, 7); // Couleur blanche
                        }
                        else
                        {
                            std::cout << 'O';
                        }
                    }

                    if (this->m_plateau[ligne][paire * 2].EstVide())
                    {
                        std::cout << char(couleur1);
                    }
                }
                else
                {
                    std::cout << char(couleur1);
                }
            }
            // Seconde case de la paire
            for (int subColonne = 0; subColonne < CASE; subColonne++)
            {
                if (interligne == 1 && subColonne == 3)
                {
                    if (this->m_plateau[ligne][paire * 2 + 1].EstNoir())
                    {
                        if (m_plateau[ligne][paire * 2 + 1].EstDame())
                        {
                            SetConsoleTextAttribute(hConsole, 8); // Couleur gris fonce
                            std::cout << 'X';
                            SetConsoleTextAttribute(hConsole, 7); // Couleur blanche
                        }
                        else
                        {
                            std::cout << 'X';
                        }
                    }

                    if (this->m_plateau[ligne][paire * 2 + 1].EstBlanc())
                    {
                        if (m_plateau[ligne][paire * 2 + 1].EstDame())
                        {
                            SetConsoleTextAttribute(hConsole, 8); // Couleur gris fonce
                            std::cout << 'O';
                            SetConsoleTextAttribute(hConsole, 7); // Couleur blanche
                        }
                        else
                        {
                            std::cout << 'O';
                        }
                    }

                    if (this->m_plateau[ligne][paire * 2 + 1].EstVide())
                    {
                        std::cout << char(couleur2);
                    }
                }
                else
                {
                    std::cout << char(couleur2);
                }
            }
        }

        if (1 == interligne)
        {
            std::cout << "   " << ligne + 1;
        }
        std::cout << "\n";
    }
}

Position Dames::nouvelEmplacementCase(const Joueur& joueur)
{
    int ligne = 0;
    int colonne = 0;

    while (true)
    {
        while (ligne > 8 || ligne < 1)
        {
            std::cout << "Selectionner ou deplacer: Entrer le numero de ligne\n" << std::endl;
            std::cin >> ligne;
        }

        while (colonne > 8 || colonne < 1)
        {
            std::cout << "Selectionner ou deplacer: Entrer le numero de colonne\n" << std::endl;
            std::cin >> colonne;
        }

        ligne -= 1;
        colonne -= 1;

        if (this->m_plateau[ligne][colonne].EstVide())
        {
            Position position = Position(ligne, colonne);
            return position;
        }
        else
        {
            std::cout << "Le deplacement n'est pas possible.\n";
            ligne = 0;
            colonne = 0;
        }
    }
}

void Dames::ViderCase(const Position& position)
{
    this->m_plateau[position.getLigne()][position.getColonne()].ViderCase();
}
