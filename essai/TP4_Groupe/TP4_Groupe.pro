TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        case.cpp \
        dames.cpp \
        grillemorpion.cpp \
        jeu.cpp \
        joueur.cpp \
        lancement.cpp \
        morpion.cpp \
        position.cpp \
        puissance4.cpp

HEADERS += \
    case.h \
    dames.h \
    grillemorpion.h \
    jeu.h \
    joueur.h \
    lancement.h \
    position.h \
    puissance4.h
